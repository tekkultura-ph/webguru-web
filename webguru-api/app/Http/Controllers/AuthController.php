<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use JWTAuth;
use JWTAuthException;

class AuthController extends Controller {

    private function getToken($email, $password) {
        $token = null;
        //$credentials = $request->only('email', 'password');
        try {
            if (!$token = JWTAuth::attempt( ['email'=>$email, 'password'=>$password])) {
                return response()->json([
                    'response' => 'error',
                    'message' => 'Password or email is invalid',
                    'token'=>$token
                ]);
            }
        } catch (JWTAuthException $e) {
            return response()->json([
                'response' => 'error',
                'message' => 'Token creation failed',
            ]);
        }
        return $token;
    }

    public function facebookLogin(Request $request) {
        $payload = [
            'facebookId'=> $request->facebookId,
            'name'=> $request->name,
            'email'=> $request->email,
            'avatar'=> $request->avatar,
            'accessToken'=> $request->accessToken,
            'password'=>\Hash::make($request->password)
        ];
        
        $user = User::where('email', '=', $request->email)->first();
        if ($user === null) {
            $user = new \App\User($payload);

            if ($user->save()) {
                $token = self::getToken($request->email, $request->password); // generate user token
                if (!is_string($token))  return response()->json(['success'=>false,'data'=>'Token generation failed'], 201);
                
                $user = \App\User::where('email', $request->email)->get()->first();
            
                $user->access_token = $token; // update user token
            
                $user->save();
            
                $response = ['success'=>true, 'data'=>['name'=>$user->name,'id'=>$user->id,'email'=>$request->email,'access_token'=>$token]];        
            } else
                $response = ['success'=>false, 'data'=>'Couldnt register user'];
                return response()->json($response, 201);
        } else {
            $token = self::getToken($request->email, $request->password); // generate user token
            if (!is_string($token))  return response()->json(['success'=>false,'data'=>'Token generation failed'], 201);

            $user->access_token = $token; // update user token
            $user->save();
            $response = ['success'=>true, 'message'=>'User Registered Successfully.', 'data'=>['id'=>$user->id,'access_token'=>$user->access_token,'name'=>$user->name, 'email'=>$user->email]];           
          
            return response()->json($response, 201);
        }
    }

    public function getUserDetails (Request $request) {
        $token = $request->header('Authorization');
        try {
            if (!$user = JWTAuth::toUser($token)) {
                return response()->json(['code' => 404, 'message' => 'User not found.']);
            } else {

                $user = JWTAuth::toUser($token);
                return response()->json(['code' => 200, 'data' => ['user' => $user]]);
            }
        } catch (Exception $e) {
            return response()->json(['code' => 404, 'message' => 'Something went wrong']);
        }
    }

    public function logout (Request $request) {
        $user = User::where('email', '=', $request->email)->first();
        $token = self::getToken($request->email, $request->password); // generate user token
        if (!is_string($token))  return response()->json(['success'=>false,'data'=>'Token generation failed'], 201);

        $user->access_token = $token; // update user token
        $user->save();

        return response()->json([
            'code' => 200,
            'message' => 'Successfully logged out',
            'data' => $user
        ]);
    }
}