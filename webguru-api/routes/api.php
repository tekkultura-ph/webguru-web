<?php

use Illuminate\Http\Request;

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('fb-login', 'AuthController@facebookLogin');

    Route::group([
      'middleware' => 'jwt.auth'
    ], function() {
        Route::post('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@getUserDetails');
    });
});
